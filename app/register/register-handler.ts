import { Router } from "express";
import userService from "../helper/user.service";
import { responseHandler } from "../utility/response.handler";

const router=Router();

router.get("/",(req,res,next)=>{
    try{
        const result=userService.findAll();
        console.log(result);
        res.send(new responseHandler(result));
    }
    catch(e){
        next(e);
    }
})

router.post("/signup",(req,res,next)=>{
    try{
        let user=req.body;
        const result=userService.createOne(user);
        res.send(new responseHandler(result));
    }
    catch(e){
        next(e);
    }
})

router.post("/signin",(req,res,next)=>{
    try{
        let user=req.body;
        const result=userService.findOne(user);
        res.send(new responseHandler(result));
    }
    catch(e){
        next(e);
    }
})
export default router;