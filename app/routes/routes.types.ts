import { Router } from "express";

export type Routes=Route[];

export class Route{
    constructor(
        public path:string,
        public router:Router
    ){}
}