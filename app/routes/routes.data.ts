import { Route, Routes } from "./routes.types";
import RegisterRouter from "../register/register-handler";

export const routes:Routes=[
    new Route("/users",RegisterRouter),
]