import { UserSchema } from "./user.schema";
import { User, Users } from "./user.type";

const createOne=(user:User)=>UserSchema.createOne(user);
const findAll=():Users=>UserSchema.findAll();
const findOne=(user:User)=>UserSchema.findOne(user);

export default {
    createOne,
    findAll,
    findOne
}