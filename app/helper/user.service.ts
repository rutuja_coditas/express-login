import userRepo from "./user.repo";
import { UserResponses } from "./user.responses";
import { User, Users } from "./user.type";

const createOne=(user:User)=>{
    userRepo.createOne(user);
    return UserResponses.USER_REGISTERED;
}

const findAll=():Users=>{
    return userRepo.findAll();
}

const findOne=(user:User)=>{
    let isPresent=userRepo.findOne(user);
    if(isPresent)return UserResponses.USER_LOGGED_IN;
    throw UserResponses.USER_NOT_FOUND;
}

export default {
    createOne,
    findAll,
    findOne
}