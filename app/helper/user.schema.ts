import { v4 } from "uuid";
import { User, Users } from "./user.type";

export class UserSchema{

    private static users:Users=[];
    
    static findAll():Users{
        return UserSchema.users;
    }
    static createOne(user:User):User{
        let userRecord={...user,id:v4()};
        UserSchema.users.push(userRecord);
        return userRecord;
    }
    static findOne(user:User):boolean{
        let index=UserSchema.users.findIndex(x=>x.username===user.username && x.password===user.password)
        if(index===-1) return false;
        return true;
    }
}

