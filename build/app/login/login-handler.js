"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_service_1 = __importDefault(require("../helper/user.service"));
const response_handler_1 = require("../utility/response.handler");
const router = (0, express_1.Router)();
router.post("/", (req, res, next) => {
    try {
        let user = req.body;
        const result = user_service_1.default.findOne(user);
        res.send(new response_handler_1.responseHandler(result));
    }
    catch (e) {
        next(e);
    }
});
exports.default = router;
