"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_repo_1 = __importDefault(require("./user.repo"));
const user_responses_1 = require("./user.responses");
const createOne = (user) => {
    user_repo_1.default.createOne(user);
    return user_responses_1.UserResponses.USER_REGISTERED;
};
const findAll = () => {
    return user_repo_1.default.findAll();
};
const findOne = (user) => {
    let isPresent = user_repo_1.default.findOne(user);
    if (isPresent)
        return user_responses_1.UserResponses.USER_LOGGED_IN;
    throw user_responses_1.UserResponses.USER_NOT_FOUND;
};
exports.default = {
    createOne,
    findAll,
    findOne
};
