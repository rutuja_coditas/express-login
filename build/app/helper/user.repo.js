"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_schema_1 = require("./user.schema");
const createOne = (user) => user_schema_1.UserSchema.createOne(user);
const findAll = () => user_schema_1.UserSchema.findAll();
const findOne = (user) => user_schema_1.UserSchema.findOne(user);
exports.default = {
    createOne,
    findAll,
    findOne
};
