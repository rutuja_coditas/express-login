"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = void 0;
const uuid_1 = require("uuid");
class UserSchema {
    static findAll() {
        return UserSchema.users;
    }
    static createOne(user) {
        let userRecord = Object.assign(Object.assign({}, user), { id: (0, uuid_1.v4)() });
        UserSchema.users.push(userRecord);
        return userRecord;
    }
    static findOne(user) {
        let index = UserSchema.users.findIndex(x => x.username === user.username && x.password === user.password);
        if (index === -1)
            return false;
        return true;
    }
}
exports.UserSchema = UserSchema;
UserSchema.users = [];
