"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.routes = void 0;
const routes_types_1 = require("./routes.types");
const register_handler_1 = __importDefault(require("../register/register-handler"));
exports.routes = [
    new routes_types_1.Route("/users", register_handler_1.default),
];
